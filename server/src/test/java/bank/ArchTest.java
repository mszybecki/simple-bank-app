package bank;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.Test;

public class ArchTest {

    @Test
    public void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClass = new ClassFileImporter()
                .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
                .importPackages("io.owd");

        ArchRuleDefinition.noClasses()
                .that()
                .resideInAnyPackage("..service..")
                .or()
                .resideInAnyPackage("..repository..")
                .should().dependOnClassesThat()
                .resideInAnyPackage("..bank.web..")
                .because("Services and repositories should not depend on web layer")
                .check(importedClass);
    }

}
