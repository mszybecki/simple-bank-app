package bank.service;

import bank.BankApp;
import bank.entity.User;
import bank.helper.DatabaseHelper;
import bank.repository.UserRepository;
import bank.service.dto.UserDTO;
import bank.web.rest.UserResource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(classes = BankApp.class)
@DirtiesContext
public class UserServiceIntegrationTest {

    private static final String DEFAULT_LOGIN = "test-login";
    private static final String DEFAULT_PASSWORD = "test-password";

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DatabaseHelper databaseHelper;

    private UserDTO userDTO;

    @BeforeEach
    public void init() {
        userDTO = new UserDTO();
        userDTO.setLogin(DEFAULT_LOGIN);
        userDTO.setPassword(DEFAULT_PASSWORD);
    }

    @AfterEach
    public void clearDB() {
        databaseHelper.clearDB();
    }

    @Test
    public void testThatCorrectlySave() {
        long beforeSave = userRepository.findAll().size();
        userService.save(userDTO);
        long afterSave = userRepository.findAll().size();

        assertThat(beforeSave + 1).isEqualTo(afterSave);
        User user = userRepository.findAll().get(0);
        System.out.println(user.getLogin());
        System.out.println(DEFAULT_LOGIN);
        assertThat(user.getLogin()).isEqualTo(DEFAULT_LOGIN);
    }

    @Test
    public void testThatUserExists() {
        userService.save(userDTO);

        boolean test = userService.checkIfExists(userDTO);
        assertTrue(test);
    }

    @Test
    public void testThatUserNotExist() {
        userService.save(userDTO);
        userDTO.setPassword("bad password");
        boolean test = userService.checkIfExists(userDTO);
        assertFalse(test);
    }

    @Test
    public void testThatCorrectlyAddedMoney() {
        userService.save(userDTO);
        userService.addMoney(DEFAULT_LOGIN, 100L);

        User user = userRepository.findByLogin(DEFAULT_LOGIN);
        assertThat(user.getMoney()).isEqualTo(100L);
    }

    @Test
    public void testThatCorrectlyMinusMoney() {
        userService.save(userDTO);
        userService.minusMoney(DEFAULT_LOGIN, 100L);

        User user = userRepository.findByLogin(DEFAULT_LOGIN);
        assertThat(user.getMoney()).isEqualTo(-100L);
    }

    @Test
    public void testThatCorrectlyGetMoney() {
        userService.save(userDTO);
        userService.getMoney(DEFAULT_LOGIN);

        User user = userRepository.findByLogin(DEFAULT_LOGIN);
        assertThat(user.getMoney()).isEqualTo(0L);
    }
}
