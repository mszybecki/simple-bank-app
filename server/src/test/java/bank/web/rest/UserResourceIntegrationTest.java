package bank.web.rest;

import bank.BankApp;
import bank.config.security.JwtTokenProvider;
import bank.entity.User;
import bank.helper.DatabaseHelper;
import bank.helper.TestUtil;
import bank.repository.UserRepository;
import bank.service.UserService;
import bank.service.dto.UserDTO;
import bank.service.mapper.UserMapper;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;


@SpringBootTest(classes = BankApp.class)
@DirtiesContext
public class UserResourceIntegrationTest {

    private static final String DEFAULT_LOGIN = "test-login";
    private static final String DEFAULT_PASSWORD = "test-password";

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DatabaseHelper databaseHelper;

    private UserDTO userDTO;

    private UserResource userResource;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private Validator validator;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc restUserMockMvc;

    private User user;

    @Autowired
    private UserMapper userMapper;

    private static final Logger log = LoggerFactory.getLogger(UserResourceIntegrationTest.class);

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);

        final UserResource userResource = new UserResource(authenticationManager, jwtTokenProvider, userService);
        restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource)
                .setValidator(validator)
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
                .build();

        user = new User();
        user.setLogin(DEFAULT_LOGIN);
        user.setPassword(DEFAULT_PASSWORD);
    }

    @AfterEach
    public void clearDB() {
        databaseHelper.clearDB();
    }

    @Test
    @Transactional
    public void testThatCorrectlyCreatedUser() throws Exception {
        long before = userRepository.count();

        log.info("{}", userMapper.toDto(user));

        restUserMockMvc.perform(MockMvcRequestBuilders.post("/api/user/signup")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userMapper.toDto(user))))
            .andExpect(MockMvcResultMatchers.status().isCreated());

        long after = userRepository.count();
        Assertions.assertThat(after).isEqualTo(before + 1);
    }

    @Test
    @Transactional
    public void testThatLoginIsRequired() throws Exception {
        long before = userRepository.count();

        user.setLogin(null);
        log.info("{}", userMapper.toDto(user));

        restUserMockMvc.perform(MockMvcRequestBuilders.post("/api/user/signup")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userMapper.toDto(user))))
            .andExpect(MockMvcResultMatchers.status().isBadRequest());

        long after = userRepository.count();
        Assertions.assertThat(after).isEqualTo(before);
    }

    @Test
    @Transactional
    public void testThatPasswordIsRequired() throws Exception {
        long before = userRepository.count();

        user.setPassword(null);
        log.info("{}", userMapper.toDto(user));

        restUserMockMvc.perform(MockMvcRequestBuilders.post("/api/user/signup")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userMapper.toDto(user))))
            .andExpect(MockMvcResultMatchers.status().isBadRequest());

        long after = userRepository.count();
        Assertions.assertThat(after).isEqualTo(before);
    }

    @Test
    @Transactional
    public void testThatCorrectlySignin() throws Exception {
        userService.save(userMapper.toDto(user));

        restUserMockMvc.perform(MockMvcRequestBuilders.post("/api/user/signin")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userMapper.toDto(user))))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("token")));
    }

    @Test
    @Transactional
    public void testThatCorrectlyAddMoney() throws Exception {
        userService.save(userMapper.toDto(user));

        String token = jwtTokenProvider.createToken(DEFAULT_LOGIN);

        restUserMockMvc.perform(MockMvcRequestBuilders.post("/api/user/add-money")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(100L))
            .header("Authorization", "Bearer " + token))
            .andExpect(MockMvcResultMatchers.status().isNoContent());

        User user = userRepository.findByLogin(DEFAULT_LOGIN);
        Assertions.assertThat(user.getMoney()).isEqualTo(100L);
    }

    @Test
    @Transactional
    public void testThatCorrectlyMinusMoney() throws Exception {
        userService.save(userMapper.toDto(user));

        String token = jwtTokenProvider.createToken(DEFAULT_LOGIN);

        restUserMockMvc.perform(MockMvcRequestBuilders.post("/api/user/minus-money")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(100L))
            .header("Authorization", "Bearer " + token))
            .andExpect(MockMvcResultMatchers.status().isNoContent());

        User user = userRepository.findByLogin(DEFAULT_LOGIN);
        Assertions.assertThat(user.getMoney()).isEqualTo(-100L);
    }

    @Test
    @Transactional
    public void testThatCorrectlyGetMoney() throws Exception {
        userService.save(userMapper.toDto(user));

        String token = jwtTokenProvider.createToken(DEFAULT_LOGIN);

        restUserMockMvc.perform(MockMvcRequestBuilders.get("/api/user/get-money")
            .header("Authorization", "Bearer " + token))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("0")));
    }
}
