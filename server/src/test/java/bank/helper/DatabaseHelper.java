package bank.helper;

import bank.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseHelper {

    @Autowired
    private UserRepository userRepository;

    public void clearDB() {
        userRepository.deleteAll();
    }
}
