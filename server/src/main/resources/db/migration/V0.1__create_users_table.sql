create table users
(
    id bigint auto_increment primary key,
    login varchar(50) not null unique,
    password varchar(255) not null,
    money bigint null
);