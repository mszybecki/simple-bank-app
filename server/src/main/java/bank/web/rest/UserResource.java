package bank.web.rest;

import bank.config.security.JwtTokenProvider;
import bank.repository.UserRepository;
import bank.service.UserService;
import bank.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/user")
public class UserResource {

    private static final Logger log = LoggerFactory.getLogger(UserResource.class);

    private AuthenticationManager authenticationManager;

    private JwtTokenProvider jwtTokenProvider;

    private UserService userService;

    public UserResource(
            AuthenticationManager authenticationManager,
            JwtTokenProvider jwtTokenProvider,
            UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping("signup")
    public ResponseEntity singup(
            @RequestBody @Validated UserDTO userDTO
    ) throws Exception {
        log.debug("REST request to save User : {}", userDTO);
        if (userDTO.getId() != null) {
            throw new Exception("A new user cannot already have an ID");
        }
        userService.save(userDTO);
        return ResponseEntity.created(new URI("")).build();
    }

    @PostMapping("signin")
    public ResponseEntity<Map<Object, Object>> signin(
            @RequestBody @Validated UserDTO userDTO
    ) throws Exception {
        log.debug("REST request to create new token : {}", userDTO);

        try {
            String username = userDTO.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, userDTO.getPassword()));
            String token = jwtTokenProvider.createToken(username);

            Map<Object, Object> model = new HashMap<>();
            model.put("token", token);
            return ResponseEntity.ok(model);
        } catch (Exception e) {
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }

    @PostMapping("add-money")
    public ResponseEntity<Void> addMoney(
            @RequestBody Long money,
            Authentication authentication
    ) {
        userService.addMoney(authentication.getName(), money);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("minus-money")
    public ResponseEntity<Void> minusMoney(
            @RequestBody Long money,
            Authentication authentication
    ) {
        userService.minusMoney(authentication.getName(), money);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("get-money")
    public ResponseEntity<Long> getMoney(Authentication authentication) {
        return ResponseEntity.ok(
                userService.getMoney(
                        authentication.getName()));
    }
}
