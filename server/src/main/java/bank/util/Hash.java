package bank.util;

public final class Hash {

    private static int rounds = 12;

    public static String hash(String str) {
        return BCrypt.hashpw(str, BCrypt.gensalt(12));
    }

    public static boolean checkMatch(String str, String hashed) {
        return BCrypt.checkpw(str, hashed);
    }
}
