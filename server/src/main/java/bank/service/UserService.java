package bank.service;

import bank.entity.User;
import bank.repository.UserRepository;
import bank.service.dto.UserDTO;
import bank.service.mapper.UserMapper;
import bank.util.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService {

    private final static Logger log = LoggerFactory.getLogger(UserService.class);

    private UserMapper userMapper;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    public UserService(UserMapper userMapper, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void save(UserDTO dto) {
        log.debug("Request to save User : {}", dto);

        User user = userMapper.toEntity(dto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setMoney(0L);
        userRepository.save(user);
    }

    public boolean checkIfExists(UserDTO dto) {
        log.debug("Request to check if exists: {}", dto);

        User user = userRepository.findByLogin(dto.getLogin());
        if (user == null) {
            return false;
        }

        return Hash.checkMatch(dto.getPassword(), user.getPassword());
    }

    public void addMoney(String login, Long money) {
        User user = userRepository.findByLogin(login);
        user.setMoney(user.getMoney() + money);
        userRepository.save(user);
    }

    public void minusMoney(String login, Long money) {
        User user = userRepository.findByLogin(login);
        user.setMoney(user.getMoney() - money);
        userRepository.save(user);
    }

    public Long getMoney(String login) {
        return userRepository.findByLogin(login).getMoney();
    }
}
