package bank.menu;

import bank.handler.AddMoneyHandler;
import bank.handler.MinusMoneyHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Test for {@link Menu}
 */
public class MenuTest {

    private Menu menu;

    private Scanner scanner;

    @BeforeEach
    public void init() {
        InputStream stream = new ByteArrayInputStream("0\n1\n2\n".getBytes());
        scanner = new Scanner(stream);
        menu = new Menu(scanner);
    }

    @Test
    public void testThatCorrectlyChooseHandler() {
        AddMoneyHandler addMoneyHandler = Mockito.mock(AddMoneyHandler.class);
        MinusMoneyHandler minusMoneyHandler = Mockito.mock(MinusMoneyHandler.class);

        menu.add(addMoneyHandler);
        menu.add(minusMoneyHandler);

        menu.action();

        Mockito.verify(addMoneyHandler, Mockito.atLeastOnce()).handle();
        Mockito.verify(minusMoneyHandler, Mockito.never()).handle();
    }

    @Test
    public void testThatCorrectlyChooseHandlers() {
        AddMoneyHandler addMoneyHandler = Mockito.mock(AddMoneyHandler.class);
        MinusMoneyHandler minusMoneyHandler = Mockito.mock(MinusMoneyHandler.class);

        menu.add(addMoneyHandler);
        menu.add(minusMoneyHandler);

        menu.actionLoop();

        Mockito.verify(addMoneyHandler, Mockito.atLeastOnce()).handle();
        Mockito.verify(minusMoneyHandler, Mockito.atLeastOnce()).handle();
    }
}
