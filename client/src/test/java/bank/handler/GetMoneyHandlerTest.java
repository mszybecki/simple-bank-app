package bank.handler;

import bank.request.Request;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Test for {@link GetMoneyHandler}
 */
public class GetMoneyHandlerTest {

    private GetMoneyHandler getMoneyHandler;

    private Request request;

    @BeforeEach
    public void init() {
        request = Mockito.mock(Request.class);

        getMoneyHandler = new GetMoneyHandler(request);
    }

    @Test
    public void testThatCorrectlyAddedMoney() {
        Mockito.doAnswer(invocationOnMock -> {
            ResponseEntity<String> response = new ResponseEntity<>("100", HttpStatus.OK);
            return response;
        }).when(request).get(Mockito.any(), Mockito.any());

        getMoneyHandler.handle();

        Mockito.verify(request, Mockito.atLeastOnce()).get(Mockito.any(), Mockito.any());
    }
}
