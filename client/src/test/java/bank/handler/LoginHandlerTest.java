package bank.handler;

import bank.handler.dto.TokenDTO;
import bank.menu.Menu;
import bank.request.Request;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Test for {@link LoginHandler}
 */
public class LoginHandlerTest {

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    private LoginHandler loginHandler;

    private ApplicationContext context;

    private Scanner scanner;

    private Request request;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        InputStream stream = new ByteArrayInputStream("test\ntest\n3\n".getBytes());

        scanner = new Scanner(stream);
        request = Mockito.mock(Request.class);
        context = Mockito.mock(ApplicationContext.class);

        loginHandler = new LoginHandler(scanner, request, context);
    }

    @Test
    public void testThatCorrectlyLogin() {
        Mockito.doAnswer(invocationOnMock -> {
            TokenDTO tokenDTO = new TokenDTO();
            tokenDTO.setToken("test-token");
            return new ResponseEntity<>(tokenDTO, HttpStatus.OK);
        }).when(request).post(Mockito.any(), Mockito.any(), Mockito.any());

        Mockito.doAnswer(invocationOnMock -> Mockito.mock(Menu.class))
                .when(context).getBean(Menu.class);

        loginHandler.handle();

        Mockito.verify(request, Mockito.atLeastOnce()).setToken(stringCaptor.capture());
        Assertions.assertThat(stringCaptor.getValue()).isEqualTo("test-token");
    }
}
