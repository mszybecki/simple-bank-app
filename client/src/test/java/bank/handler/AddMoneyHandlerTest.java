package bank.handler;

import bank.request.Request;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Test for {@link AddMoneyHandler}
 */
public class AddMoneyHandlerTest {

    private AddMoneyHandler addMoneyHandler;

    private Scanner scanner;

    private Request request;

    @BeforeEach
    public void init() {
        InputStream stream = new ByteArrayInputStream("300\n".getBytes());

        scanner = new Scanner(stream);
        request = Mockito.mock(Request.class);

        addMoneyHandler = new AddMoneyHandler(scanner, request);
    }

    @Test
    public void testThatCorrectlyAddedMoney() {
        Mockito.doAnswer(invocationOnMock -> new ResponseEntity<>(HttpStatus.NO_CONTENT))
                .when(request).post(Mockito.any(), Mockito.any(), Mockito.any());

        addMoneyHandler.handle();

        Mockito.verify(request, Mockito.atLeastOnce()).post(Mockito.any(), Mockito.any(), Mockito.any());
    }
}
