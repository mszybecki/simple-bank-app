package bank.handler;

import bank.handler.dto.TokenDTO;
import bank.menu.Menu;
import bank.request.Request;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;

/**
 * Test for {@link RegisterHandler}
 */
public class RegisterHandlerTest {

    private RegisterHandler registerHandler;

    private Scanner scanner;

    private Request request;

    @BeforeEach
    public void init() {
        InputStream stream = new ByteArrayInputStream("test\ntest\n".getBytes());

        scanner = new Scanner(stream);
        request = Mockito.mock(Request.class);

        registerHandler = new RegisterHandler(scanner, request);
    }

    @Test
    public void testThatCorrectlyLogin() {
        Mockito.doAnswer(invocationOnMock -> new ResponseEntity<>(HttpStatus.CREATED))
                .when(request).post(Mockito.any(), Mockito.any(), Mockito.any());

        registerHandler.handle();

        Mockito.verify(request, Mockito.atLeastOnce()).post(Mockito.any(), Mockito.any(), Mockito.any());
    }
}
