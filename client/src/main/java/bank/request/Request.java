package bank.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class Request {

    private RestTemplate restTemplate;

    private String serverURL;

    private String token;

    @Autowired
    public Request(
            RestTemplate restTemplate,
            @Value("${server.url}") String serverURL) {
        this.restTemplate = restTemplate;
        this.serverURL = serverURL;
        this.token = "";
    }

    public <T> ResponseEntity<T> post(String url, Object request, Class<T> responseType) {
        HttpEntity<Object> entity = new HttpEntity<>(request, this.getHeaders());
        return this.restTemplate.postForEntity(this.serverURL + url, entity, responseType);
    }

    public <T> ResponseEntity<T> get(String url, Class<T> responseType) {
        HttpEntity entity = new HttpEntity(this.getHeaders());
        return this.restTemplate.exchange(this.serverURL + url, HttpMethod.GET, entity, responseType);
    }

    public void setToken(String token) {
        this.token = token;
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();

        if (!this.token.equals("")) {
            headers.add("Authorization", "Bearer " + this.token);
        }

        return headers;
    }
}
