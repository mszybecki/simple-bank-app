package bank.handler;

import bank.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

@Component
public class RegisterHandler implements Handler{

    private Scanner scanner;

    private Request request;

    private final static Logger log = LoggerFactory.getLogger(RegisterHandler.class);

    @Autowired
    public RegisterHandler(
            Scanner scanner,
            Request request) {
        this.scanner = scanner;
        this.request = request;
    }

    @Override
    public void handle() {
        System.out.print("Write login: ");
        String login = this.scanner.nextLine();

        System.out.print("Write password: ");
        String password = this.scanner.nextLine();

        Map<String, String> payload = new HashMap<>();
        payload.put("login", login);
        payload.put("password", password);

        ResponseEntity<String> response = this.request.post("/api/user/signup", payload, String.class);

        if (response.getStatusCode().value() == 201) {
            System.out.println("User created, you can now log in");
        } else {
            System.out.println("Server error");
        }
    }

    @Override
    public String message() {
        return "Register new user";
    }
}
