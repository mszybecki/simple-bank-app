package bank.handler;

import bank.handler.Handler;
import bank.request.Request;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class AddMoneyHandler implements Handler {

    private Scanner scanner;

    private Request request;

    public AddMoneyHandler(
            Scanner scanner,
            Request request) {
        this.scanner = scanner;
        this.request = request;
    }

    @Override
    public void handle() {
        System.out.print("Write how much money you want add to your account: ");
        Long money = this.scanner.nextLong();
        this.scanner.nextLine();

        ResponseEntity<Void> response = this.request.post("/api/user/add-money", money, Void.class);

        if (response.getStatusCode().value() == 204) {
            System.out.println("Money added to your account");
        }
    }

    @Override
    public String message() {
        return "Add money to your account";
    }
}
