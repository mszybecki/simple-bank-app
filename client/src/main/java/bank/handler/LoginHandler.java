package bank.handler;

import bank.handler.dto.TokenDTO;
import bank.menu.Menu;
import bank.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


@Component
public class LoginHandler implements Handler {

    private Scanner scanner;

    private Request request;

    private ApplicationContext context;

    private final static Logger log = LoggerFactory.getLogger(LoginHandler.class);

    public LoginHandler(
            Scanner scanner,
            Request request,
            ApplicationContext context) {
        this.scanner = scanner;
        this.request = request;
        this.context = context;
    }

    @Override
    public void handle() {
        System.out.print("Write login: ");
        String login = this.scanner.nextLine();

        System.out.print("Write password: ");
        String password = this.scanner.nextLine();

        Map<String, String> payload = new HashMap<>();
        payload.put("login", login);
        payload.put("password", password);

        ResponseEntity<TokenDTO> response = this.request.post("/api/user/signin", payload, TokenDTO.class);

        if (response.getStatusCode().value() == 403) {
            System.out.println("Bad credentials");
        } else if (response.getStatusCode().value() == 200) {
            this.request.setToken(response.getBody().getToken());

            Menu menu = context.getBean(Menu.class);
            menu.add(context.getBean(GetMoneyHandler.class));
            menu.add(context.getBean(AddMoneyHandler.class));
            menu.add(context.getBean(MinusMoneyHandler.class));
            menu.actionLoop();
        } else {
            System.out.println("Server error");
        }
    }

    @Override
    public String message() {
        return "Login";
    }
}
