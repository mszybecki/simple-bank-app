package bank.handler;

import bank.request.Request;
import org.springframework.stereotype.Component;

@Component
public class GetMoneyHandler implements Handler {

    private Request request;

    public GetMoneyHandler(Request request) {
        this.request = request;
    }

    @Override
    public void handle() {
        System.out.println(this.request.get("/api/user/get-money", String.class).getBody());
    }

    @Override
    public String message() {
        return "See how much money you have money";
    }
}
