package bank.handler;

public interface Handler {

    public void handle();

    public String message();
}
