package bank.handler;

import bank.request.Request;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class MinusMoneyHandler implements Handler {

    private Scanner scanner;

    private Request request;

    public MinusMoneyHandler(
            Scanner scanner,
            Request request) {
        this.scanner = scanner;
        this.request = request;
    }

    @Override
    public void handle() {
        System.out.print("Write how much money you want to pull from your account: ");
        Long money = this.scanner.nextLong();
        this.scanner.nextLine();

        ResponseEntity<Void> response = this.request.post("/api/user/minus-money", money, Void.class);

        if (response.getStatusCode().value() == 204) {
            System.out.println("Money pulled to your account");
        }
    }

    @Override
    public String message() {
        return "Pull money from your account";
    }
}
