package bank.menu;

import bank.handler.Handler;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
@Scope("prototype")
public class Menu {

    private List<Handler> handlers;

    private Scanner scanner;

    public Menu(Scanner scanner) {
        this.handlers = new ArrayList<>();
        this.scanner = scanner;
    }

    public void add(Handler handler) {
        this.handlers.add(handler);
    }

    public void action() {
        for (int i = 0; i < this.handlers.size(); i++) {
            System.out.println("[" + i + "] " + this.handlers.get(i).message());
        }

        System.out.print("Chose action: ");
        int action = this.scanner.nextInt();
        this.scanner.nextLine();

        if (action >= 0 && action < this.handlers.size()) {
            this.handlers.get(action).handle();
        }
    }

    public void actionLoop() {
        while (true) {
            for (int i = 0; i < this.handlers.size(); i++) {
                System.out.println("[" + i + "] " + this.handlers.get(i).message());
            }

            System.out.print("Chose action: ");
            int action = this.scanner.nextInt();
            this.scanner.nextLine();

            if (action >= 0 && action < this.handlers.size()) {
                this.handlers.get(action).handle();
            } else {
                break;
            }
        }
    }
}
