package bank;

import bank.config.ApplicationProperties;
import bank.handler.LoginHandler;
import bank.handler.RegisterHandler;
import bank.menu.Menu;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class BankApp implements CommandLineRunner {

    private ApplicationContext context;

    public BankApp(ApplicationContext context) {
        this.context = context;
    }

    public static void main(String[] args) {
        SpringApplication.run(BankApp.class);
    }

    @Override
    public void run(String... args) throws Exception {
        Menu menu = this.context.getBean(Menu.class);
        menu.add(this.context.getBean(LoginHandler.class));
        menu.add(this.context.getBean(RegisterHandler.class));
        menu.action();
    }
}
